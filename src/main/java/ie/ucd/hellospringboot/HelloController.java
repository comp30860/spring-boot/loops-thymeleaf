package ie.ucd.hellospringboot;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
    @GetMapping("/colours")
    public String colours( Model model) {
        ArrayList<String> colours = new ArrayList<String>();
        colours.add("red");
        colours.add("green");
        colours.add("blue");
        model.addAttribute("colours", colours);
        return "hello";
    }
}